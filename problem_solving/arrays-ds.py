# 6 X 6array, hour glass shape total count, get max of it
def hourglassSum(arr):
    # fixed size
    R = 6
    C = 6
    max_sum = -5000
    # base conditions
    if R<3 or C<3:
        return "Not possible"
    
    #we can not form hour glass with last 2 rows and colomns
    for i in range(R-2):
        for j in range(C-2):
            # forming hourglass
            sum = arr[i][j]+arr[i][j+1]+arr[i][j+2] + arr[i+1][j+1] + arr[i+2][j]+ arr[i+2][j+1]+ arr[i+2][j+2]
            if sum > max_sum:
                max_sum = sum
            else:
                continue
    return max_sum


#q1 = 1 x y === append integer y to arr[(x ^ last_answer)%n]
#q2 = 2 x y === idx=x ^ last_answer)%n, assign last_answer to arr[idx][y%size(arr[idx])] and store it in arr
# n = no.of empty array to initialize=2, last_answer=0
# so n = [[],[]]  here 1st [] is for query1, 2nd one [] is for query 2  
# 
# queries : 1 0 5  means , 1: query type, x=0, y=5
def dynamicquery(n,queries):
    last_answer = 0
    ans = [[] for _ in range(n)]
    #ans = [[]]*2
    print(len(ans))
    print(len(ans[0]))
    res = []
    for query_type,x,y in queries:
        idx = (x ^ last_answer)%n
        if query_type == 1:
            ans[idx].append(y)
        else:
            v = y % len(ans[idx])
            last_answer = ans[idx][v]
            res.append(last_answer)

    print(res)

if __name__ == "__main__":
    print("hi")
    '''
    hourglassSum_arr = [[-9,-9,-9,1,1,1],
            [0,-9,0,4,3,2],
            [-9,-9,-9,1,2,3],
            [0,0,8,6,6,0],
            [0,0,0,-2,0,0],
            [0,0,1,2,4,0]]
    print(hourglassSum(hourglassSum_arr))
    '''
    n=2
    queries = [[1, 0, 5],
                [1,1,7],
                [1,0, 3],
                [2,1, 0],
                [2,1 ,1]]
    dynamicquery(n,queries)
